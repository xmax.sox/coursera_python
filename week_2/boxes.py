a, b, c, a1, b1, c1 = int(input()), int(input()), \
                      int(input()), int(input()), \
                      int(input()), int(input())

if (a1 == a and b1 == b and c1 == c) or \
        (a1 == b and b1 == a and c1 == c) or \
        (a1 == c and b1 == b and c1 == a) or \
        (a1 == a and b1 == c and c1 == b) or \
        (a1 == b and b1 == c and c1 == a) or \
        (a1 == c and b1 == a and c1 == b):
    print('Boxes are equal')
elif (a1 <= a and b1 <= b and c1 <= c) or \
        (a1 <= b and b1 <= a and c1 <= c) or \
        (a1 <= c and b1 <= b and c1 <= a) or \
        (a1 <= a and b1 <= c and c1 <= b) or \
        (a1 <= b and b1 <= c and c1 <= a) or \
        (a1 <= c and b1 <= a and c1 <= b):
    print('The first box is larger than the second one')
elif (a1 >= a and b1 >= b and c1 >= c) or \
        (a1 >= b and b1 >= a and c1 >= c) or \
        (a1 >= c and b1 >= b and c1 >= a) or \
        (a1 >= a and b1 >= c and c1 >= b) or \
        (a1 >= b and b1 >= c and c1 >= a) or \
        (a1 >= c and b1 >= a and c1 >= b):
    print('The first box is smaller than the second one')
else:
    print('Boxes are incomparable')
