def OlimpResult(A):
    return sorted(A, key=lambda x: x[1], reverse=True)


n = int(input())
A = []

for i in range(n):
    s = list(input().split())  # ["Ivanov", "10"]
    s[1] = int(s[1])
    A.append(s)

for elem in OlimpResult(A):
    print(elem[0])
