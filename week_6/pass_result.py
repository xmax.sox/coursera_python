def pass_result(k, list_n):
    if len(list_n) <= k:
        return 0
    elif list_n[0] == list_n[k]:
        return 1
    for i in range(k, 0, -1):
        if list_n[i] < list_n[i - 1]:
            return list_n[i - 1]


inFile = open('input.txt', 'r', encoding='utf8')
k = int(inFile.readline())
list_n = []
for line in inFile:
    a, b, c = [int(i) for i in line.split()[-3:]]
    if a >= 40 and b >= 40 and c >= 40:
        list_n.append(a + b + c)
inFile.close()
list_n.sort(reverse=True)
print(pass_result(k, list_n))
