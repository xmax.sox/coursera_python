S, N = map(int, input().split())
date_list = sorted([int(input()) for _ in range(N)])
sum_list = sum(date_list)
while sum_list > S and N:
    sum_list -= date_list.pop()
    N -= 1
print(N)
