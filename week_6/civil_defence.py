def task(inp):
    N, C, M, B = inp

    C = list(sorted(enumerate(C), key=lambda x: x[1]))
    B = list(sorted(enumerate(B), key=lambda x: x[1]))
    CRes = list(range(N))

    z = 0
    for k in range(N):
        x = 9999999999
        y = 0
        a = C[k][0]
        for h in range(z, M):
            if abs(C[k][1] - B[h][1]) < x:
                x = abs(C[k][1] - B[h][1])
                y = B[h][0] + 1
                z = h
            else:
                break
        CRes[a] = y

    return CRes


if __name__ == '__main__':

    n = int(input())
    nlist = list(map(int, input().split()))
    m = int(input())
    mlist = list(map(int, input().split()))

    data = [
        n,
        nlist,
        m,
        mlist
    ]

    result = task(data)

    print(*result)
