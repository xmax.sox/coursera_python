def CountSort(A):
    n = len(A)
    k = 101
    C = []

    for i in range(k):
        C.append(0)
    for i in A:
        C[i] += 1
    j = 0
    for i in range(k):
        for k in range(C[i]):
            A[j] = i
            j += 1
    return A


if __name__ == '__main__':
    A = list(map(int, input().split()))
    r = CountSort(A)
    print(' '.join([str(i) for i in r]))
