def merge(a, b):
    i = 0
    j = 0
    new_list = []
    while i < len(a) and j < len(b):
        if a[i] <= b[j]:
            new_list.append(a[i])
            i += 1
        else:
            new_list.append(b[j])
            j += 1
    while i < len(a):
        new_list.append(a[i])
        i += 1
    while j < len(b):
        new_list.append(b[j])
        j += 1
    return new_list


a = list(map(int, input().split()))
b = list(map(int, input().split()))
print(*merge(a, b))
