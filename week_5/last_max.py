n = [int(i) for i in input().split()]
maxN = max(n)
x = 0
for i in range(len(n)-1, 0, -1):
    if n[i] == maxN:
        x = i
        break
print(maxN, x)
