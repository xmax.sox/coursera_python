def IsPrime(n, d=2):
    if n == 1:
        return False
    while d*d <= n:
        if n % d == 0:
            return False
        d += 1
    return True


n = int(input())
print("YES" if IsPrime(n) else "NO")
