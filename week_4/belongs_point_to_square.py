first = 1
second = -1


def IsPointInSquare(x, y):
    return (x <= first and x >= second) and \
           (y >= second and y <= first)


x = float(input())
y = float(input())
if IsPointInSquare(x, y):
    print('YES')
else:
    print('NO')
