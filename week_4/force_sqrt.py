def force_s(a, b):
    if a % 2 == 0:
        return (a ** 2) ** (b / 2)
    else:
        return a * (a ** (b - 1))


a = float(input())
b = int(input())
print(force_s(a, b))
